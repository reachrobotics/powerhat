#	MekaMon Berserker Robot Embedded Software Documentation

This documentation refers to the Mekamon Smart Module Power Board.

-----------------------------------------------------------------------------
	
##	Project Overview

**Project Name:**       	Smart Module Mekamon Power Board

A small board taking up as few of the Raspberry Pi header pins as possible.

This board does provide a header to access the Raspberry Pi UART and I2C pins.
Header J4 has these connections as well as power and is laid out thus:

```
 _______________________________________________
|			|			|			|    		| 
|			|			|			|	 		|
|	 GND	|	UART	|	UART	|	 V+		|
|			|	 TX		|	 RX		|	 		| 
|___________|___________|___________|___________|
|			|			|			|    		|
|			|	 I2C	|	 I2C	|	 		|
|	 GND	|	 SDA	|	 SCL	|	 V+		|	J4
|			|			|			|	 		| 
|___________|___________|___________|___________|
 
```
 
 By default the power board V+ provided by the breakout header is set to +3.3V
 by resistor R2. If the user wishes to provide +5V to this header, remove R2
 and bridge the left two pins of Header J3.

```
 ___________________________________
|			|			|			|
|			|  			|		 	|
|	 +5V	|   Header	|	+3V3	|	J3
|			|	  V+	|			|
|___________|___________|___________|

```
 


-----------------------------------------------------------------------------

##	Author Roles

For this project, the following git user roles are used.

### Project 
**Role:** 					Owner
**User:**					Reach Robotics Limited

### Research and Development Engineer
**Role:**					Circuit design
**User:** 					Dr. Anthony J. Portelli

###	Research and Development Engineer
**Role:**					Test Engineer
**User:**					Wesley Freeman

-----------------------------------------------------------------------------
	
## 	Supplementary Documentation

Please contact wesley@reachrobotics.com for any additional information




